package test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class SeleniumTest {

	private WebDriver driver;

//	@Before
//	public void beforeTest() {
//
//		System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
//		driver = new ChromeDriver();
//
//		WebDriverWait wait = new WebDriverWait(driver, 10);
//
//	}

	@Before
	public void beforeTest() {

		System.setProperty("webdriver.ie.driver", ".\\driver\\IEDriverServer.exe");
		driver = new InternetExplorerDriver();



	}

	@Test
	public void open() throws InterruptedException {
		Thread.sleep(3000);

		//Thread.sleep(3000);
		driver.get("https://www.google.co.jp/");

		Thread.sleep(3000);

		System.out.println(driver.getPageSource());
	}

	@After
	public void afterTest() {
		driver.quit();
	}
}
